const { ok } = require('assert')
const randomDigitOneThroughNine = require('.')

for (let i = 0; i < 1000; i++) {
  const actual = randomDigitOneThroughNine()
  ok(!Number.isNaN(actual))
  ok(actual >= 1)
  ok(actual <= 9)
}
