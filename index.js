const LOWEST_DIGIT = 1
const HIGHEST_DIGIT = 9

module.exports = function randomDigitOneThroughNine () {
  const range = HIGHEST_DIGIT - LOWEST_DIGIT
  const nonce = Math.random() * range
  const integer = Math.floor(nonce)
  const shifted = LOWEST_DIGIT + integer
  return shifted
}
