# random-digit-one-through-nine

Returns a random digit from 1 through 9.

## Usage

```js
const digit = randomDigitOneThroughNine()
```

## See Also

- [three-random-digits-one-through-nine](https://www.npmjs.com/package/three-random-digits-one-through-nine)
- https://twitter.com/simon_swain/status/1044466605181231104
